document.addEventListener('DOMContentLoaded', () => {
  const svgObject = document.getElementById('office-layout');
  svgObject.addEventListener('load', () => {
    const svgDoc = svgObject.contentDocument;

    const desks = svgDoc.querySelectorAll('rect');
    desks.forEach(desk => {
      desk.addEventListener('click', () => {
        const deskId = desk.id;
        fetch(`/api/desks/book`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({ deskId })
        })
        .then(response => response.json())
        .then(data => {
          if (data.success) {
            desk.setAttribute('fill', 'green');
            alert('Desk booked successfully!');
          } else {
            alert('Failed to book desk');
          }
        });
      });
    });
  });
});

