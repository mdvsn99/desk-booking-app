const request = require('supertest');
const express = require('express');
const app = require('../server');

describe('GET /', function() {
  it('respond with html', function(done) {
    request(app)
      .get('/')
      .expect('Content-Type', /html/)
      .expect(200, done);
  });
});

