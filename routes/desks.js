const express = require('express');
const router = express.Router();
const db = require('../models/database');

// Buchung eines Schreibtisches
router.post('/book', (req, res) => {
  const { deskId } = req.body;
  
  const stmt = db.prepare('UPDATE desks SET available = 0 WHERE id = ?');
  stmt.run(deskId, function(err) {
    if (err) {
      return res.status(500).json({ success: false });
    }
    res.json({ success: true });
  });
});

module.exports = router;

