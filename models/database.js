const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(':memory:');

db.serialize(() => {
  // Tabellen erstellen
  db.run("CREATE TABLE users (id INTEGER PRIMARY KEY, name TEXT, email TEXT)");
  db.run("CREATE TABLE desks (id INTEGER PRIMARY KEY, location TEXT, available BOOLEAN)");
});

module.exports = db;

